

import net.serenitybdd.jbehave.SerenityStories;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import steps.MySteps;


public class CalculationTest extends SerenityStories{

    @Steps
    MySteps steps;

    @Given("number $number")
    public void storeTheNumber(int number){
        steps.number(number);
    }

    @When("multiply on $number")
    public void multipleOn(int number){
        steps.multipleNumber(number);
    }

    @Then("result should be $result")
    public void result(int expResult){
        steps.result(expResult);
    }
}
