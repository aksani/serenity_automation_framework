package steps;

import net.thucydides.core.annotations.Step;
import org.junit.Assert;


public class MySteps{

    private int givenNumber;

    @Step("Given a number {0}")
    public void number(int number){
        givenNumber = number;
    }

    @Step("When multiply on {0}")
    public void multipleNumber(int number){
        givenNumber *= number;
    }

    @Step("Then result should be {0}")
    public void result(int result){
        Assert.assertEquals("",givenNumber, result);
    }
}
